#pragma once
#include "Soldier.h"
#include "Board.h"

class Pawn : public Soldier
{
public:
	Pawn();
	~Pawn();

	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};

