#pragma once
#include "Soldier.h"
#include "Board.h"

class Queen : public Soldier
{
public:
	Queen();
	~Queen();


	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};
