#include "Pawn.h"
#include <cmath>

Pawn::Pawn()
{
}

Pawn::~Pawn()
{
}



char Pawn::getType()
{
	if (this->color == BLACK) {
		return 'p';
	}
	return 'P';
}

bool Pawn::canMove(std::string s, Soldier*** b)
{
	int old1 = int(s[0]) - SML_START;
	int old2 = int(s[1]) - ZERO_ASCII;
	int new1 = int(s[2]) - SML_START;
	int new2 = int(s[3]) - ZERO_ASCII;

	int bOld2 = int(s[0]) - SML_START;
	int bOld1 = 8 - (int(s[1]) - ZERO_ASCII);
	int bNew2 = int(s[2]) - SML_START;
	int bNew1 = 8 - (int(s[3]) - ZERO_ASCII);

	Soldier* dst = b[bNew1][bNew2];

	// check color of pawn
	if (this->color == BLACK) {
		// check basic movement of black pawn
		if (old1 == new1)
		{
			// check normal movement of black pawn
			if ((old2 - new2) == 1){
				// if any soldier in dst
				if (dst == NULL) {
					return true;
				}
			}
			// check starting available movement of black pawn
			else if ((old2 - new2) == 2) {
				// if any soldier at dst, some soldier is in the way of this pawn and if the pawn is at the start of(black situation)
				if (old2 == 7 && dst == NULL && b[bOld1 + 1][bOld2] == NULL) {
					return true;
				}
			}
		}
		// check eat movement of black pawn
		else if (abs(new1 - old1) == (old2 - new2) && abs(new1 - old1) == 1) {
			// if color different
			if (dst != NULL && this->color != dst->color) {
				return true;
			}
		}
	}
	else if (this->color == WHITE) {
		// check basic movement of white pawn
		if (old1 == new1)
		{
			// check normal movement of white pawn
			if ((new2 - old2) == 1) {
				// if any soldier in dst
				if (dst == NULL) {
					return true;
				}
			}
			// check starting available movement of white pawn
			else if ((new2 - old2) == 2) {
				// if any soldier at dst, some soldier is in the way of this pawn and if the pawn is at the start of(white situation)
				if (old2 == 2 && dst == NULL && b[bOld1 - 1][bOld2] == NULL) {
					return true;
				}
			}
		}
		// check eat movement of white pawn
		else if (abs(new1 - old1) == (new2 - old2) && abs(new1 - old1) == 1) {
			// if color different
			if (dst != NULL && this->color != dst->color) {
				return true;
			}
		}
	}
	return false;
}
