#pragma once
#include "Soldier.h"
#include "Board.h"

class King : public Soldier
{
public:
	King();
	~King();


	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};

