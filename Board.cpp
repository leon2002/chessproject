#include "Board.h"
#include "Soldier.h"
#include "King.h"
#include "Rook.h"
#include "Queen.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include <cmath>






Board::Board(std::string s)
{
	this->blackKingEaten = false;
	this->whiteKingEaten = false;
	this->checkWhite = false;
	this->checkBlack = false;
	char ch;
	int i = 0, j = 0, index = 0;

	boardArr = new Soldier * *[COLS];

	for (i = 0; i < ROWS; i++)
	{

		boardArr[i] = new Soldier * [ROWS];
	}

	// initialization of board
	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			ch = s[index];
			if ((int(ch) >= CAP_START && int(ch) <= CAP_END) || (int(ch) >= SML_START && int(ch) <= SML_END)) {

				switch (ch) {
				case 'k':
					this->boardArr[i][j] = new King();
					break;
				case 'q':
					this->boardArr[i][j] = new Queen();
					break;
				case 'r':
					this->boardArr[i][j] = new Rook();
					break;
				case 'n':
					this->boardArr[i][j] = new Knight();
					break;
				case 'b':
					this->boardArr[i][j] = new Bishop();
					break;
				case 'p':
					this->boardArr[i][j] = new Pawn();
					break;

				case 'K':
					this->boardArr[i][j] = new King();
					break;
				case 'Q':
					this->boardArr[i][j] = new Queen();
					break;
				case 'R':
					this->boardArr[i][j] = new Rook();
					break;
				case 'N':
					this->boardArr[i][j] = new Knight();
					break;
				case 'B':
					this->boardArr[i][j] = new Bishop();
					break;
				case 'P':
					this->boardArr[i][j] = new Pawn();
					break;
				}

				if (int(ch) <= CAP_END) {
					this->boardArr[i][j]->color = WHITE;
					ch = char(int(ch) + (SML_START - CAP_START));
				}
				else {
					this->boardArr[i][j]->color = BLACK;
				}

			}
			else {
				this->boardArr[i][j] = NULL;
			}
			index++;
		}
	}
}

Board::~Board()
{
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			delete this->boardArr[i][j];
		}
		delete[] this->boardArr[i];
	}
	delete[] this->boardArr;
}

int Board::move(std::string s)
{
	int winBlack = 0;
	int winWhite = 0;
	int old2 = int(s[0]) - SML_START;
	int old1 = 8 - (int(s[1]) - ZERO_ASCII);
	int new2 = int(s[2]) - SML_START;
	int new1 = 8 - (int(s[3]) - ZERO_ASCII);

	int bOld2 = int(s[0]) - SML_START;
	int bOld1 = 8 - (int(s[1]) - ZERO_ASCII);
	int bNew2 = int(s[2]) - SML_START;
	int bNew1 = 8 - (int(s[3]) - ZERO_ASCII);

	int bKingBlack1 = 0, bKingBlack2 = 0, bKingWhite1 = 0, bKingWhite2 = 0;
	char kingBlack1 = ' ', kingBlack2 = ' ', kingWhite1 = ' ', kingWhite2 = ' ';
	std::string sKingBlack = "", sKingWhite = "";

	char s1 = ' ', s2 = ' ';
	std::string sCur = "";

	Soldier* temp;

	bool checkCurrCheckBlack = false;
	bool checkCurrCheckWhite = false;

	bool isLegalWhiteCheck = false;
	bool isLegalBlackCheck = false;


	if (old1 == new1 && old2 == new2) {
		return 7;
	}
	if ((old1 >= 0 && old1 < 8) && (new1 >= 0 && new1 < 8) && (old2 >= 0 && old2 < 8) && (new2 >= 0 && new2 < 8)) {
		// check turn and which color play
		if (this->boardArr[old1][old2] != NULL && this->boardArr[old1][old2]->color == this->getTurn()) {
			// if any specific soldier can move or eat
			if (this->boardArr[old1][old2]->canMove(s, this->boardArr)) {
				temp = this->boardArr[new1][new2];
				this->boardArr[new1][new2] = this->boardArr[old1][old2];
				this->boardArr[old1][old2] = NULL;


				if (this->checkBlack) {
					isLegalBlackCheck = true;
				}
				if (this->checkWhite) {
					isLegalWhiteCheck = true;
				}

				// check if check exist in current board
				for (int i = 0; i < ROWS; i++) {
					for (int j = 0; j < COLS; j++) {
						if (this->boardArr[i][j] != NULL) {
							if (this->boardArr[i][j]->getType() == 'k') {
								bKingBlack1 = i;
								bKingBlack2 = j;
								winWhite++;
							}
							else if (this->boardArr[i][j]->getType() == 'K') {
								bKingWhite1 = i;
								bKingWhite2 = j;
								winBlack++;
							}
						}
					}
				}
				if (!winBlack)
				{
					this->whiteKingEaten = true;
				}
				if (!winWhite)
				{
					this->blackKingEaten = true;
				}
				// build index for black and white kings
				kingBlack2 = (bKingBlack1) * (-1) + 8 + ZERO_ASCII;
				kingWhite2 = (bKingWhite1) * (-1) + 8 + ZERO_ASCII;

				kingBlack1 = bKingBlack2 + SML_START;
				kingWhite1 = bKingWhite2 + SML_START;

				sKingBlack += kingBlack1;
				sKingBlack += kingBlack2;

				sKingWhite += kingWhite1;
				sKingWhite += kingWhite2;

				// check if any soldier can eat it's negative color king
				for (int i = 0; i < ROWS; i++) {
					for (int j = 0; j < COLS; j++) {
						s1 = j + SML_START;
						s2 = (i) * (-1) + 8 + ZERO_ASCII;
						sCur += s1;
						sCur += s2;

						if (this->boardArr[i][j] != NULL) {
							sCur += sKingWhite;
							if (this->boardArr[i][j]->canMove(sCur, this->boardArr)) {
								this->checkWhite = true;
								checkCurrCheckWhite = true;
							}
						}

						sCur = "";
						sCur += s1;
						sCur += s2;

						if (this->boardArr[i][j] != NULL) {
							sCur += sKingBlack;
							if (this->boardArr[i][j]->canMove(sCur, this->boardArr)) {
								this->checkBlack = true;
								checkCurrCheckBlack = true;
							}
						}
						sCur = "";
					}
				}
				if (!checkCurrCheckBlack) {
					this->checkBlack = false;
				}
				if (!checkCurrCheckWhite) {
					this->checkWhite = false;
				}


				if ((this->checkBlack || this->checkWhite) && ((!(isLegalBlackCheck)) && (!(isLegalWhiteCheck)))) {
					if ((this->checkBlack && this->boardArr[new1][new2]->color == WHITE) || (this->checkWhite && this->boardArr[new1][new2]->color == BLACK)) {
						changeTurn(this->turn);
						return 1;
					}
					else {
						if (this->boardArr[new1][new2]->color == BLACK) {
							this->checkBlack = false;
						}
						if (this->boardArr[new1][new2]->color == WHITE) {
							this->checkWhite = false;
						}

						this->boardArr[old1][old2] = this->boardArr[new1][new2];
						this->boardArr[new1][new2] = temp;
						return 4;

					}
				}
				else {
					changeTurn(this->turn);
					return 0;
				}

			}
			else {
				if (this->boardArr[new1][new2] == NULL) {
					return 6;
				}
				else if (this->boardArr[old1][old2]->color == this->boardArr[new1][new2]->color) {
					return 3;
				}
				
			}
		}
		else {
			return 2;
		}
	}
	else {
		return 5;
	}
}

int Board::getTurn()
{
	return this->turn;
}

void Board::changeTurn(int turn)
{
	if (turn == BLACK) {
		this->turn = WHITE;
	}
	else {
		this->turn = BLACK;
	}
}

void Board::setTurn(int turn)
{
	this->turn = turn;
}