#include "Knight.h"

Knight::Knight()
{
}

Knight::~Knight()
{
}


char Knight::getType()
{
	if (this->color == BLACK) {
		return 'n';
	}
	return 'N';
}

bool Knight::canMove(std::string s, Soldier*** b)
{
	int old1 = int(s[0]) - SML_START;
	int old2 = int(s[1]) - ZERO_ASCII;
	int new1 = int(s[2]) - SML_START;
	int new2 = int(s[3]) - ZERO_ASCII;

	int bOld2 = int(s[0]) - SML_START;
	int bOld1 = 8 - (int(s[1]) - ZERO_ASCII);
	int bNew2 = int(s[2]) - SML_START;
	int bNew1 = 8 - (int(s[3]) - ZERO_ASCII);

	Soldier* dst = b[bNew1][bNew2];


	// check basic movement of knight
	if (abs(abs(old1 - new1) - abs(old2 - new2)) == 1 && ((abs(old1 - new1) == 1 || abs(old1 - new1) == 2) && (abs(old2 - new2) == 1 || abs(old2 - new2) == 2)) && (!(old1 == old2 && new1 == new2))) {
		// is any soldier in dst or color different(can move)
		if (dst == NULL || this->color != dst->color) {
			return true;
		}
	}
	return false;
}
