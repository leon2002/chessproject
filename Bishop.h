#pragma once
#include "Soldier.h"
#include "Board.h"

class Bishop : public Soldier
{
public:
	Bishop();
	~Bishop();

	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};
