#pragma once
#include <string>

#define BLACK 1
#define WHITE 0

class Soldier
{
public:
	int color; // 1 - black, 0 - white

	Soldier();
	virtual ~Soldier();

	virtual bool canMove(std::string s, Soldier*** b) = 0;
	virtual char getType() = 0;

};

