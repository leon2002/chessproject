#pragma once
#include "Soldier.h"
#include <string.h>
#include <iostream>

#define ROWS 8
#define COLS 8

#define CAP_START 65
#define CAP_END 90
#define SML_START 97
#define SML_END 122

#define ZERO_ASCII 48

#define BLACK 1
#define WHITE 0

#define TURN_INDEX 64



class Board
{
public:
	Soldier*** boardArr;
	int turn;
	bool checkWhite;
	bool checkBlack;
	bool blackKingEaten;
	bool whiteKingEaten;


	Board(std::string s);
	~Board();
	int move(std::string s);
	int getTurn();
	void changeTurn(int turn);
	void setTurn(int turn);




};