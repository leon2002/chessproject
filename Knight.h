#pragma once
#include "Soldier.h"
#include "Board.h"

class Knight : public Soldier
{
public:
	Knight();
	~Knight();

	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};
