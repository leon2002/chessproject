#include "Queen.h"

Queen::Queen()
{
}

Queen::~Queen()
{
}



char Queen::getType()
{
	if (this->color == BLACK) {
		return 'q';
	}
	return 'Q';
}

bool Queen::canMove(std::string s, Soldier*** b)
{
	int old1 = int(s[0]) - SML_START;
	int old2 = int(s[1]) - ZERO_ASCII;
	int new1 = int(s[2]) - SML_START;
	int new2 = int(s[3]) - ZERO_ASCII;

	int bOld2 = int(s[0]) - SML_START;
	int bOld1 = 8 - (int(s[1]) - ZERO_ASCII);
	int bNew2 = int(s[2]) - SML_START;
	int bNew1 = 8 - (int(s[3]) - ZERO_ASCII);

	Soldier* dst = b[bNew1][bNew2];

	int i = bOld1, j = bOld2;
	bool flag = true;

	// check basic movement of queen
	if (((abs(old1 - new1) == abs(old2 - new2)) || (old1 == new1 || old2 == new2)) && (!(old1 == old2 && new1 == new2))) {
		// is any soldier in dst or color different and checks if some soldier is in the way of this queen
		if (dst == NULL || this->color != dst->color) {
			if (new2 > old2) {
				if (new1 > old1) {
					while (i != bNew1 && j != bNew2) {
						i--;
						j++;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
				else if (new1 < old1) {
					while (i != bNew1 && j != bNew2) {
						i--;
						j--;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
			}
			else if (new2 < old2) {
				if (new1 > old1) {
					while (i != bNew1 && j != bNew2) {
						i++;
						j++;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
				else if (new1 < old1) {
					while (i != bNew1 && j != bNew2) {
						i++;
						j--;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
			}
			if (old1 == new1) {
				if (new2 > old2) {
					while (i != bNew1) {
						i--;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
				else if (new2 < old2) {
					while (i != bNew1) {
						i++;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
			}
			else if (old2 == new2) {
				if (new1 > old1) {
					while (j != bNew2) {
						j++;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
				else if (new1 < old1) {
					while (j != bNew2) {
						j--;
						if (b[i][j] != NULL && b[i][j] != dst) {
							flag = false;
						}
					}
				}
			}
		}
		else {
			flag = false;
		}
	}
	else {
		flag = false;
	}
	return flag;
}
