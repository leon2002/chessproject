#pragma once
#include "Soldier.h"
#include "Board.h"

class Rook : public Soldier
{
public:
	Rook();
	~Rook();


	char getType();

	// Inherited via Soldier
	virtual bool canMove(std::string s, Soldier*** b) override;
};
