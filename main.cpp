#include "Board.h"
#include "Soldier.h"
#include "King.h"
#include "Rook.h"
#include "Queen.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include <iostream>



int main() {
	std::string sBoard = "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1";
	std::string sMove = "";
	Board b(sBoard);
	char ch;
	b.setTurn(int(sBoard[TURN_INDEX]) - ZERO_ASCII);
	bool win = false;
	while (!win)
	{
		ch = '8';
		std::cout << "Current board:" << std::endl;
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {

				if (b.boardArr[i][j] != NULL) {
					std::cout << b.boardArr[i][j]->getType() << "   " << std::ends;
				}
				else {
					std::cout << "#   " << std::ends;
				}
			}
			std::cout << "      " << ch << std::endl;
			ch--;
		}
		std::cout << std::endl;
		std::cout << std::endl;
		for (int i = 0; i < 8; i++) {
			ch = i + SML_START;
			std::cout << ch << "   " << std::ends;

		}
		std::cout << std::endl;
		if (b.checkBlack == true) {
			std::cout << "check on black king!" << std::endl;
		}
		else if (b.checkWhite == true) {
			std::cout << "check on white king!" << std::endl;
		}
		if (b.getTurn() == BLACK) {
			std::cout << "Enter movement(BLACK) : " << std::ends;
		}

		else {
			std::cout << "Enter movement(WHITE) : " << std::ends;
		}
		std::cin >> sMove;
		// movement : "e2e4"
		b.move(sMove);

		if (b.blackKingEaten)
		{
			std::cout << "the white won!" << std::ends;
			win = true;
		}
		else if (b.whiteKingEaten)
		{
			std::cout << "the black won!" << std::ends;
			win = true;
		}
	}
}